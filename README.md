# HA code assessment

## Assumptions
The directions were not quite clear in a couple of places.  Noted assumptions:

searching by business hours means complete match of DoW, open, close
geographic location means operating city rather than address
filter by jobs means filter by worktype
"setting of data accepts JSON" implies an api to create new businesses

## Local Setup

### Docker/Docker-compose

#### Requirements
- docker 19.03.12 or greate
- docker-compose 1.25.0 or creater

#### Startup commands
- `docker-compose up`

### Local, non-docker

#### Requirements
- ruby 2.7.1
- bundler 2.1.4
- yarn 1.22.4

#### Startup Comands
From within repo directory

- `bundle i`
- `yarn install`
- `rails s`

## Testing Api Calls
Get all businesses:

`curl --location --request GET 'http://localhost:3000/businesses'`

Get all businessses sorted by name desc

`curl --location --request GET 'http://localhost:3000/businesses?sort_direction=desc'`

Get all businesses sorted by rating desc

`curl --location --request GET 'http://localhost:3000/businesses?sort_by=rating&sort_direction=desc'`

Get businesses filter by name

`curl --location --request GET 'http://localhost:3000/businesses?businessName=Sample%20Business%20%231'`

Get businesses filter by hours

`curl --location --request GET 'http://localhost:3000/businesses?businessHours[dow]=Monday&businessHours[open]=9&businessHours[close]=5'`

Get businesses filter by work type

`curl --location --request GET 'http://localhost:3000/businesses?location'`

Get businesses filter by location

`curl --location --request GET 'http://localhost:3000/businesses?location=Commerce%20City'`

Get businesses filter by rating

`curl --location --request GET 'http://localhost:3000/businesses?rating=4'`

Post new business
```
curl --location --request POST 'http://localhost:3000/businesses' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
    "businessName:": "Sample Business #4",
    "businessHours": [
      {
        "dayOfWeek": "Monday",
        "open": 9,
        "close": 5
      },
      {
        "dayOfWeek": "Tuesday",
        "open": 9,
        "close": 5
      },
      {
        "dayOfWeek": "Wednesday",
        "open": 9,
        "close": 5
      },
      {
        "dayOfWeek": "Thursday",
        "open": 9,
        "close": 5
      },
      {
        "dayOfWeek": "Friday",
        "open": 9,
        "close": 5
      }
    ],
    "businessAddress": {
      "addressLine1": "1234 Fake St",
      "addressLine2": "Suite 500",
      "city": "Denver",
      "stateAbbr": "CO",
      "postal": "80210"
    },
    "operatingCities": [
      "Denver",
      "Lakewood",
      "Thornton",
      "Golden",
      "Arvada",
      "Centennial",
      "Parker"
    ],
    "workTypes": [
      "Maid Service",
      "House Cleaning",
      "Moving Services"
    ],
    "reviews": [
      {
        "ratingScore": 4.5,
        "customerComment": "Use them weekly to clean our home. Do a great job every time"
      },
      {
        "ratingScore": 4,
        "customerComment": "Helped us move homes, very timely"
      },
      {
        "ratingScore": 4,
        "customerComment": "On time, did a good job"
      }
    ]
  }'
```