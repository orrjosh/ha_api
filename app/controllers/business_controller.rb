class BusinessController < ApplicationController
  skip_before_action :verify_authenticity_token  # danger will robinson 
  def index
    @businesses = Business.where(nil)
    @businesses = @businesses.filter_by_business_name(params[:businessName]) if params[:businessName].present?  
    @businesses = @businesses.filter_by_business_hours(params[:businessHours]) if params[:businessHours].present?
    @businesses = @businesses.filter_by_work_type(params[:workType]) if params[:workType].present?
    @businesses = @businesses.filter_by_location(params[:location]) if params[:location].present?
    @businesses = @businesses.filter_by_rating(params[:rating]) if params[:rating].present?
    @businesses = business_sort(@businesses, sort_by = params[:sort_by] || "business_name", direction = params[:sort_direction] || "asc")
    render json: @businesses
  end
  def create
    b = Business.new
    b.create_from_json(request.raw_post)
    render status: 200, json: []
  end
  # this is not ideal, sorts set after query
  def business_sort(businesses, sort_field, direction)
    if sort_field == "rating"
      sort_by_rating(businesses, direction)
    else
      sort_by_name(businesses, direction)
    end
  end

  def sort_by_name(businesses, direction)
    businesses = businesses.sort_by { |b| b["business_name"]}
    if(direction == "desc")
      businesses.reverse!
    end
    businesses
  end
  def sort_by_rating(businesses, direction)
    businesses = businesses.sort_by { |b| b.rating}
    if(direction == "desc")
      businesses.reverse!
    end
    businesses
  end
end