class ReviewSerializer < ActiveModel::Serializer
  attributes :rating_score, :customer_comment
end
