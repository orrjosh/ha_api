class BusinessAddressSerializer < ActiveModel::Serializer
  attributes :address_line_1, :address_line_2, :city, :state_abbr, :postal
end
