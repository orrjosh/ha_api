class BusinessSerializer < ActiveModel::Serializer
  attributes :business_name, :operating_cities, :work_types, :rating
  has_many :business_hours
  has_one :business_address
  has_many :reviews
  
  def work_types
    JSON.parse(object.work_types) if object.work_types
  end

  def operating_cities
    JSON.parse(object.operating_cities) if object.operating_cities
  end
end
