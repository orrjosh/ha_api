class BusinessHourSerializer < ActiveModel::Serializer
  attributes :day_of_week, :open, :close
end
