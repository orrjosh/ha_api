class Business < ApplicationRecord
  has_many :business_hours
  has_one :business_address
  has_many :reviews

  scope :filter_by_business_name, -> (business_name) {where business_name: business_name}
  # collapse next three filters to one, nil check things
  scope :filter_by_business_hours, -> (business_hours) do
    joins(:business_hours).where(business_hours: {open: business_hours[:open], close: business_hours[:close], day_of_week: business_hours[:dow]}).uniq
  end
  scope :filter_by_work_type, -> (work_type) do
    where("work_types LIKE ?", "%#{work_type}%")
  end
  scope :filter_by_location, -> (location) do
    where("operating_cities LIKE ?", "%#{location}%")
  end
  scope :filter_by_rating , -> (rating) do
    joins(:reviews).group("business_id").having('AVG(reviews.rating_score) >= ?', rating.to_d)
  end

  def rating
    @rating = self.reviews.average(:rating_score)
  end

  def create_from_json(business_json_string)
    jb = JSON.parse(business_json_string)
    b = Business.new
    b.business_name = jb["businessName:"]
    # business hours
    b.business_hours = []
    jb["businessHours"].each{ |jbh|
      bh = BusinessHour.new
      bh.day_of_week = jbh["dayOfWeek"]
      bh.open = jbh["open"]
      bh.close = jbh["close"]
      b.business_hours.push(bh)
    }
    # business address
    b.business_address = BusinessAddress.new
    b.business_address.address_line_1 = jb["businessAddress"]["addressLine1"]
    b.business_address.address_line_2 = jb["businessAddress"]["addressLine2"]
    b.business_address.city = jb["businessAddress"]["city"]
    b.business_address.state_abbr = jb["businessAddress"]["stateAbbr"]
    b.business_address.postal = jb["businessAddress"]["postal"]
    # operating cities
    b.operating_cities = jb["operatingCities"]
    # work types
    b.work_types = jb["workTypes"]
    # reviews
    b.reviews = []
    jb["reviews"].each do |jr|
      r = Review.new
      r.rating_score = jr["ratingScore"]
      r.customer_comment= jr["customerComment"]
      b.reviews.push(r)
    end
    b.save
  end
end
