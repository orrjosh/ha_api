class CreateBusinessAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :business_addresses do |t|
      t.string :address_line_1
      t.string :address_line_2
      t.string :city
      t.string :state_abbr
      t.string :postal
      t.belongs_to :business, null: false, foreign_key: true

      t.timestamps
    end
  end
end
