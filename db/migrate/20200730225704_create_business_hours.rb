class CreateBusinessHours < ActiveRecord::Migration[6.0]
  def change
    create_table :business_hours do |t|
      t.string :day_of_week
      t.integer :open
      t.integer :close
      t.belongs_to :business, null: false, foreign_key: true

      t.timestamps
    end
  end
end
