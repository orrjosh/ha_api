class AddWorkTypesToBusiness < ActiveRecord::Migration[6.0]
  def change
    add_column :businesses, :work_types, :text
  end
end
