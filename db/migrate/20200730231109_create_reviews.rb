class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews do |t|
      t.decimal :rating_score
      t.text :customer_comment
      t.belongs_to :business, null: false, foreign_key: true

      t.timestamps
    end
  end
end
