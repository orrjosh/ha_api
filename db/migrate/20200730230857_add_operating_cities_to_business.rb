class AddOperatingCitiesToBusiness < ActiveRecord::Migration[6.0]
  def change
    add_column :businesses, :operating_cities, :text, array: true
  end
end
