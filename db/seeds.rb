# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


if Business.count == 0
  path = File.join(File.dirname(__FILE__), "seeds/assessment.json")
  json = JSON.parse(File.read(path))
  json.each do |jb|
    b = Business.new
    b.business_name = jb["businessName:"]
    # business hours
    b.business_hours = []  
    jb["businessHours"].each{ |jbh| 
      bh = BusinessHour.new
      bh.day_of_week = jbh["dayOfWeek"]
      bh.open = jbh["open"]
      bh.close = jbh["close"]
      b.business_hours.push(bh)
    }
    # business address
    b.business_address = BusinessAddress.new
    b.business_address.address_line_1 = jb["businessAddress"]["addressLine1"]
    b.business_address.address_line_2 = jb["businessAddress"]["addressLine2"]
    b.business_address.city = jb["businessAddress"]["city"]
    b.business_address.state_abbr = jb["businessAddress"]["stateAbbr"]
    b.business_address.postal = jb["businessAddress"]["postal"]
    # operating cities
    b.operating_cities = jb["operatingCities"]
    # work types
    b.work_types = jb["workTypes"]
    # reviews
    b.reviews = []
    jb["reviews"].each do |jr|
      r = Review.new
      r.rating_score = jr["ratingScore"]
      r.customer_comment= jr["customerComment"]
      b.reviews.push(r)
    end
    b.save
  end
end