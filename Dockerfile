FROM ruby:2.7.1

USER root
RUN apt-get update \
    && apt-get install -y nodejs npm \
    &&  rm -rf /var/cache/apt/archives \
    && npm install -g yarn
WORKDIR /usr/src

EXPOSE 3000
COPY ./ ./app/

WORKDIR /usr/src/app

#RUN ls
RUN bundle install
RUN yarn install
CMD ["rails", "s", "-b", "0.0.0.0"]
