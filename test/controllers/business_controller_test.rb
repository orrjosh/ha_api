require 'test_helper'

class BusinessControllerTest < ActionDispatch::IntegrationTest
  test "get /businesses returns all businesses" do
    get "/businesses"
    assert_response :success
    assert_equal "Business1", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?sort_by=business_name returns businesses sorted by name asc" do
    get "/businesses?sort_by=business_name"
    assert_response :success
    assert_equal "Business1", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?sort_by=business_name&sort_direction=desc returns businesses sorted by name desc" do
    get "/businesses?sort_by=business_name&sort_direction=desc"
    assert_response :success
    assert_equal "Business2", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?sort_direction=desc returns businesses sorted by name desc" do
    get "/businesses?sort_direction=desc"
    assert_response :success
    assert_equal "Business2", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?sort_by=rating returns businesses sorted by rating asc" do
    get "/businesses?sort_by=rating"
    assert_response :success
    assert_equal "Business2", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?sort_by=rating%sort_direction=desc returns businesses sorted by rating desc" do
    get "/businesses?sort_by=rating&sort_direction=desc"
    assert_response :success
    assert_equal "Business1", response.parsed_body[0]['business_name']
    assert_equal 2, response.parsed_body.length
  end
  test "get /businesses?businessName returns correct businesses" do
    get "/businesses?businessName=Business1"
    assert_response :success
    assert_equal 1, response.parsed_body.length
    assert_equal "Business1", response.parsed_body[0]['business_name']
  end
  test "get /businesses?workType returns businesses with the requested work type" do
    get "/businesses?workType=House%20Cleaning"
    assert_response :success
    assert_equal 1, response.parsed_body.length
    assert_equal "Business1", response.parsed_body[0]['business_name']
  end
  test "get /businesses?location returns businesses with the location as operating city" do
    get "/businesses?workType=House%20Cleaning"
    assert_response :success
    assert_equal 1, response.parsed_body.length
    assert_equal "Business1", response.parsed_body[0]['business_name']
  end
  test "get /businesses?rating returns businesses with >= rating" do
    get "/businesses?rating=4.5"
    assert_response :success
    assert_equal 1, response.parsed_body.length
    assert_equal "Business1", response.parsed_body[0]['business_name']
  end
  test "post to /business with json creates new business" do
    business = {
      'businessName' => "NewBusiness",
      'businessHours' => [],
      'businessAddress' => {},
      'operatingCities' => [],
      'workTypes' => [],
      'reviews' => []
    }
    post "/businesses", params: JSON[business], headers: { 'CONTENT_TYPE' => 'application/json' }
    assert_response :success
  end
end
